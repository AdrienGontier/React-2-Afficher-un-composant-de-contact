import { useState } from 'react'
import './App.css'
import Contact from './components/Contact'


function App() {
  
  return (
    
    <div className="App">
      <h1>Nom, Prénom et Statut</h1>
      <div>
        <Contact firstName="Alan" lastName="Turing" isLoggedIn={true} />
      </div>
      <div>
        <Contact firstName="Tim" lastName="Berners-Lee" />
      </div>
      <div>
        <Contact firstName="Ada" lastName="Lovelace" /> 
      </div>
      
    </div>
  )
 

}







export default App
